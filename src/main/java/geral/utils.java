package geral;

import com.aventstack.extentreports.ExtentTest;
import java.io.FileInputStream;
import java.time.LocalDateTime;
import java.util.Properties;
import java.util.Random;

import static geral.hooks.test;

public class utils
{
    protected ExtentTest extentTest;
    protected static Properties props = new Properties();
    public static String pathProperties = System.getProperty("user.dir") + "/src/test/resources/configurations.properties";
    private static LocalDateTime dataAtual;
    
    public utils()
    {
        this.extentTest = test;
    }

    public static String getProp(String var)
    {
        try
        {
            props.load(new FileInputStream(pathProperties));
            return props.getProperty(var);
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public boolean verificaStatusCode(int statusCodeEsperado, int statusCodeApi)
    {
        boolean ok = false;
        if(statusCodeEsperado == statusCodeApi)
        {
            ok = true;
            extentTest.pass("Requisição realizada com sucesso, statusCode " + statusCodeApi + "retornado");
        }
        else
        {
            System.out.println("Requisição realizada sem sucesso, statusCode retornado foi " + statusCodeApi + " quando deveria ser " + statusCodeEsperado + " .");
            extentTest.fail("Requisição realizada sem sucesso, statusCode retornado foi " + statusCodeApi + " quando deveria ser " + statusCodeEsperado + " .");
        }
        return ok;
    }

    public boolean verificaMensagem(String msg)
    {
        boolean ok = false;
        switch (msg)
        {
            case "{password=password.required}":
                ok = true;
                extentTest.pass("Mensagem de retorno da requisição [" + msg + "] validada com sucesso!");
            break;
            
            case "{username=username.required}":
	            ok = true;
	            extentTest.pass("Mensagem de retorno da requisição [" + msg + "] validada com sucesso!");
	        break;
	        
            case "{password=password.required, username=username.required}":
            	ok = true;
	            extentTest.pass("Mensagem de retorno da requisição [" + msg + "] validada com sucesso!");
	        break;

            default:
                extentTest.fail("Mensagem de retorno da requisição [" + msg + "] inexistente!");
            break;
        }

        return ok;
    }

    public static String ReturnValueList(String[] listaValue)
    {
    	Random random = new Random();
    	
    	return listaValue[random.nextInt((listaValue.length - 1))];
    }

    public static String diaAtual()
    {
    	String  dia = "";
    	dataAtual = LocalDateTime.now();

    	if(dataAtual.getDayOfMonth() < 10)
    	{
    		dia = "0" + dataAtual.getDayOfMonth();
    	}
    	else
    	{
    		dia = Integer.toString(dataAtual.getDayOfMonth());
    	}
    	
    	return dia;
    }
    
    public static String mesAtual()
    {
    	String  mes = "";
    	dataAtual = LocalDateTime.now();
    	Integer mesAtual = dataAtual.getMonth().getValue();
    	
    	if(mesAtual < 10)
    	{
    		mes = "0" + mesAtual;
    	}
    	else
    	{
    		mes = Integer.toString(mesAtual);
    	}
    	
    	return mes;
    }
    
    public static String anoAtual()
    {
    	dataAtual = LocalDateTime.now();
    	return Integer.toString(dataAtual.getYear());
    }

    public static String horaAtual()
    {
    	String hora = "";
    	dataAtual = LocalDateTime.now();
    	
    	if(dataAtual.getHour() < 10)
    	{
    		hora = "0" + Integer.toString(dataAtual.getHour());
    	}
    	else
    	{
    		hora = Integer.toString(dataAtual.getHour());
    	}
    	
    	return hora;
    }
    
    public static String minutoAtual()
    {
    	String minuto = "";
    	dataAtual = LocalDateTime.now();
    	
    	if(dataAtual.getMinute() < 10)
    	{
    		minuto = "0" + Integer.toString(dataAtual.getMinute());
    	}
    	else
    	{
    		minuto = Integer.toString(dataAtual.getMinute());
    	}
    	
    	return minuto;
    }

    public static String segundoAtual()
    {
    	String segundo = "";
    	dataAtual = LocalDateTime.now();
    	
    	if(dataAtual.getSecond() < 10)
    	{
    		segundo = "0" + Integer.toString(dataAtual.getSecond());
    	}
    	else
    	{
    		segundo = Integer.toString(dataAtual.getSecond());
    	}
    	
    	return segundo;
    }

    public static String generatorName()
    {
        String letras = "ABCDEFGHIJKLMNOPQRSTUVYWXZ";
        Random random = new Random();
        String armazenaChaves = "";
        int index = -1;
        for( int i = 0; i < 9; i++ ) {
        index = random.nextInt( letras.length() );
        armazenaChaves += letras.substring( index, index + 1 );
        }
        return armazenaChaves;
    }

    public static Integer generatorNumber(int size)
    {
        Random random = new Random();
        Integer number = 0;
        switch (size)
        {
            case 1:
                number = random.nextInt(9);
                break;

            case 2:
                number = random.nextInt(99);
                break;

            case 3:
                number = random.nextInt(999);
                break;

            case 4:
                number = random.nextInt(9999);
                break;

            case 5:
                number = random.nextInt(99999);
                break;

            case 6:
                number = random.nextInt(999999);
                break;

            case 7:
                number = random.nextInt(9999999);
                break;

            case 8:
                number = random.nextInt(99999999);
                break;

            case 9:
                number = random.nextInt(999999999);
                break;

            default:
                number = random.nextInt(99999);
                break;
        }
        return  number;
    }

    public static String generatorJob(int size)
    {
        String job = "";
        Integer number = geral.utils.generatorNumber(size);
        switch (number)
        {
            case 1:
                job = "QA";
                break;

            case 2:
                job = "Leader";
                break;

            case 3:
                job = "Tech Leader";
                break;

            case 4:
                job = "Product Owner";
                break;

            case 5:
                job = "Agile";
                break;

            case 6:
                job = "Scrum";
                break;

            case 7:
                job = "Developer";
                break;

            case 8:
                job = "Product Manager";
                break;

            case 9:
                job = "Uex";
                break;
        }

        return  job;
    }
}