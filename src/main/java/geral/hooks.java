package geral;

import java.nio.file.Paths;

import org.ccil.cowan.tagsoup.ScanHandler;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class hooks
{
    public static ExtentTest test;
    private ExtentHtmlReporter htmlReporter;
    private ExtentReports extentReporter;
    private String caminhoParaSalvarEvidenciasEReportHtml = Paths.get(System.getProperty("user.dir").toString(), "extentReports/htmlReport.html").toString();
    public static String cenario;
    
    @Before
    public void beforeCenario(Scenario scenario)
    {
    	cenario = scenario.getName();
    	
        if(extentReporter == null)
        {
            htmlReporter = new ExtentHtmlReporter(Paths.get(caminhoParaSalvarEvidenciasEReportHtml).toString());
            htmlReporter.config().setChartVisibilityOnOpen(true);
            htmlReporter.config().setDocumentTitle("PNLD Digital Automação Back");
            htmlReporter.config().setReportName("PNLD.Digital.TestReport");
            htmlReporter.config().setTestViewChartLocation(ChartLocation.TOP);
            htmlReporter.config().setTheme(Theme.STANDARD);
            htmlReporter.config().setTimeStampFormat("dd/MM/yyyy HH:mm:ss");

            extentReporter = new ExtentReports();
            extentReporter.attachReporter(htmlReporter);
        }

        test = extentReporter.createTest(scenario.getName());
    }

    @After
    public void afterCenario(Scenario scenario)
    {
        extentReporter.flush();
    }
}
