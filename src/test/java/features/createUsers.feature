# language: pt
Funcionalidade: Create users

  @CreateUsers @finalizado
  Cenario: [API Create Users | @positivo] - Realiza requisição com o payload correto de create users
    Dado que monto o payload de post da api de create users
      | payload | header   | name    | job     |
      | correto | semToken | inserir | inserir |
    Quando executo a requisicao tipo post na api
      | pathApi    | operation |
      | /api/users | post      |
    Entao valido o retorno da api
      | statusCodeEsperado |
      |                201 |

  @CreateUsers @finalizado
  Cenario: [API Create Users | @negativo] - Realiza requisição com o payload sem informar o parâmetro [name]
    Dado que monto o payload de post da api de create users
      | payload | header   | job     |
      | semName | semToken | inserir |
    Quando executo a requisicao tipo post na api
      | pathApi    | operation |
      | /api/users | post      |
    Entao valido o retorno da api
      | statusCodeEsperado |
      |                201 |

  @CreateUsers @finalizado
  Cenario: [API Create Users | @negativo] - Realiza requisição com o payload sem informar o parâmetro [job]
    Dado que monto o payload de post da api de create users
      | payload | header   | name    |
      | semJob | semToken  | inserir |
    Quando executo a requisicao tipo post na api
      | pathApi    | operation |
      | /api/users | post      |
    Entao valido o retorno da api
      | statusCodeEsperado |
      |                201 |

  @CreateUsers @finalizado
  Cenario: [API Create Users | @negativo] - Realiza requisição com o payload com o parâmetro [name] vazio
    Dado que monto o payload de post da api de create users
      | payload   | header   | name    | job     |
      | nameVazio | semToken |         | inserir |
    Quando executo a requisicao tipo post na api
      | pathApi    | operation |
      | /api/users | post      |
    Entao valido o retorno da api
      | statusCodeEsperado |
      |                201 |

  @CreateUsers @finalizado
  Cenario: [API Create Users | @negativo] - Realiza requisição com o payload com o parâmetro [job] vazio
    Dado que monto o payload de post da api de create users
      | payload  | header   | name    | job     |
      | jobVazio | semToken | inserir |         |
    Quando executo a requisicao tipo post na api
      | pathApi    | operation |
      | /api/users | post      |
    Entao valido o retorno da api
      | statusCodeEsperado |
      |                201 |

  @CreateUsers @finalizado
  Cenario: [API Create Users | @negativo] - Realiza requisição sem enviar o payload
    Dado que monto o payload de post da api de create users
      | payload    | header   | name    | job     |
      | semPayload | semToken | inserir | inserir |
    Quando executo a requisicao tipo post na api
      | pathApi    | operation |
      | /api/users | post      |
    Entao valido o retorno da api
      | statusCodeEsperado |
      |                400 |