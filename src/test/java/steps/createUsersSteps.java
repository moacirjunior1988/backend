package steps;

import java.util.List;
import java.util.Map;

import com.aventstack.extentreports.ExtentTest;
import com.google.gson.Gson;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;
import geral.hooks;
import geral.utils;
import io.restassured.RestAssured;
import object.createUsers;
import org.junit.Assert;

public class createUsersSteps
{
    protected int statusCodeApi;
    protected ExtentTest extentTest;
    private stepBase stepBase;
    private createUsers createUsers;
    geral.utils utils = new utils();

    public createUsersSteps(stepBase stepBase)
    {
        this.stepBase = stepBase;
        this.extentTest = hooks.test;
        this.createUsers = new createUsers();
        stepBase.gson = new Gson();
    }

    @Given("que monto o payload de post da api de create users$")
    public void dadoQueMontoOPayloadDePostDaApiDeCreateUsers(DataTable dataTable)
    {
        extentTest.info("Dado que monto o payload de post da api de create users");
        List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);

        switch (data.get(0).get("payload"))
        {
            case "correto":
            {
                if (data.get(0).get("name").equals("inserir")) {
                    createUsers.name = geral.utils.generatorName();
                } else if (!data.get(0).get("name").equals("inserir")) {
                    createUsers.name = data.get(0).get("name");
                }

                if (data.get(0).get("job").equals("inserir")) {
                    createUsers.job = geral.utils.generatorJob(1);
                } else if (!data.get(0).get("job").equals("inserir")) {
                    createUsers.job = data.get(0).get("job");
                }
            }
            break;

            case "semName": {
                if (data.get(0).get("job").equals("inserir")) {
                    createUsers.job = geral.utils.generatorJob(1);
                } else if (!data.get(0).get("job").equals("inserir")) {
                    createUsers.job = data.get(0).get("job");
                }
            }
            break;

            case "semJob": {
                if (data.get(0).get("name").equals("inserir")) {
                    createUsers.name = geral.utils.generatorName();;
                } else if (!data.get(0).get("name").equals("inserir")) {
                    createUsers.name = data.get(0).get("name");
                }
            }
            break;

            case "nameVazio":
            {
                createUsers.name = "";

                if (data.get(0).get("job").equals("inserir")) {
                    createUsers.job = geral.utils.generatorJob(1);
                } else if (!data.get(0).get("job").equals("inserir")) {
                    createUsers.job = data.get(0).get("job");
                }
            }
            break;

            case "jobVazio": {
                if (data.get(0).get("name").equals("inserir")) {
                    createUsers.name = geral.utils.generatorName();;
                } else if (!data.get(0).get("name").equals("inserir")) {
                    createUsers.name = data.get(0).get("name");
                }

                createUsers.job = "";
            }
            break;

            case "semPayload":
                createUsers = null;
                break;
        }

        stepBase.json = stepBase.gson.toJson(createUsers);

        System.out.println("Body montado: " + stepBase.json);

        switch (data.get(0).get("header"))
        {
            case "correto":
                stepBase.request = RestAssured.given()
                        .header("Authorization", "Bearer" + stepBase.token)
                        .contentType("application/json")
                        .body(stepBase.json);
                break;

            case "semToken":
                stepBase.request = RestAssured.given()
                        .contentType("application/json")
                        .body(stepBase.json);
                break;
        }
    }

    @When("executo a requisicao tipo post na api")
    public void quandoExecutoARequisicaoTipoPostNaApi(DataTable dataTable)
    {
        extentTest.info("Quando executo a requisicao tipo post na api");
        List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);

        try
        {
            System.out.println("Final url: " + stepBase.baseUri + data.get(0).get("pathApi"));
            switch (data.get(0).get("operation").toLowerCase())
            {
                case "get":
                    stepBase.response = stepBase.request.when().get(stepBase.baseUri + data.get(0).get("pathApi"));
                    break;

                case "post":
                    stepBase.response = stepBase.request.when().post(stepBase.baseUri + data.get(0).get("pathApi"));
                    break;

                case "put":
                    stepBase.response = stepBase.request.when().put(stepBase.baseUri + data.get(0).get("pathApi"));
                    break;

                case "delete":
                    stepBase.response = stepBase.request.when().delete(stepBase.baseUri + data.get(0).get("pathApi") + stepBase.id);
                    break;
            }
            System.out.println("Body de retorno da api: " + stepBase.response.then().extract().asString());
        }
        catch (Exception e)
        {
            extentTest.fail("Quando executo a requisição tipo post na api.");
            extentTest.warning("Erro: " + e.getMessage());
        }
    }

    @Then("valido o retorno da api")
    public void EntaoValidoORetornoDaApi(DataTable dataTable)
    {
        extentTest.info("Então valido o retorno da api");
        List<Map<String, String>> data = dataTable.asMaps(String.class, String.class);
        int statuCode = Integer.parseInt(data.get(0).get("statusCodeEsperado"));
        statusCodeApi = stepBase.response.getStatusCode();

        switch (statusCodeApi)
        {
            case 200:
                Assert.assertTrue("Falha ao validar o status code retornado pela api, verifique!", utils.verificaStatusCode(statuCode, statusCodeApi));
                break;

            case 201:
                Assert.assertTrue("Falha ao validar o status code retornado pela api, verifique!", utils.verificaStatusCode(statuCode, statusCodeApi));
                break;

            case 202:
                Assert.assertTrue("Falha ao validar o status code retornado pela api, verifique!", utils.verificaStatusCode(statuCode, statusCodeApi));
                break;

            case 204:
                Assert.assertTrue("Falha ao validar o status code retornado pela api, verifique!", utils.verificaStatusCode(statuCode, statusCodeApi));
                break;

            case 400:
                stepBase.msg = stepBase.response.then().extract().path("fieldErrors").toString();

                Assert.assertTrue("Falha ao validar o status code retornado pela api, verifique!", utils.verificaStatusCode(statuCode, statusCodeApi));
                System.out.println("Return msn: " + stepBase.msg);
                break;

            default:
                System.out.println("Status Code: " + statusCodeApi + " retornado pela api não previsto para tratamento!");
                extentTest.fail("Status Code: \" + statusCodeApi + \" retornado pela api não previsto para tratamento!");
                Assert.assertTrue(false);
                break;
        }
    }
}
