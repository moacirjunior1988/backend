package steps;

import com.google.gson.Gson;
import geral.utils;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class stepBase {
    utils util = new utils();

    public Response response;
    public String json;

    public String id;

    protected String token;
    protected Gson gson;
    protected RequestSpecification request;
    protected String baseUri = util.getProp("baseURI");
    protected String msg;
}
